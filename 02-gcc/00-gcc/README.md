# GCC 開發環境

## 輸出 a.out

```
$ gcc sum.c
```

## 使用 -o 參數

```
$ gcc sum.c -o sum
```

## gcc 的使用流程

![](./img/gccFlow.png)

