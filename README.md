# 陳鍾誠的系統程式課程

主題                         | 內容
-----------------------------|--------------------------------------------
[01-sp](01-sp)               | System Programming -- 系統程式
[02-gcc](02-gcc)             | GNU Compiler Collection -- C 語言開發工具
[03-compiler](03-compiler)   | Compiler -- 編譯器
[04-asm](04-asm)             | Assembly -- 組合語言、處理器與虛擬機
[05-obj](05-obj)             | Object Code -- 目的檔、連結與載入
[06-se](06-se)               | Software Engineering -- 軟體工程、速度與正確性
[07-lib](07-lib)             | Library -- 函式庫與平台
[08-os](08-os)               | Operating System -- 作業系統
[09-riscv](09-riscv)         | RISC-V 處理器 -- 從計算機結構到作業系統

